/*
Activity:
Create functions to solve the following problems
1. Determine if a number is divisible by 3 or 5 or both
if the number is divisible by 3, return "Fizz"
if the number is divisible by 5, return "Buzz"
if the number is divisible by 3 AND 5, return "FizzBuzz"
if the number is not divisible by 3 and 5, return "Pop"

Test cases:
5 -> Buzz
30 -> FizzBuzz
27 -> Fizz
17 -> Pop
*/

// function fzbzCalculator(num){
// 	if (num % 3 == 0) {
// 		return "Fizz";
// 	} else if (num % 5 == 0) {
// 		return "Buzz";
// 	} else if (num % 15 == 0) {  
// 		return "FizzBuzz";
// 	} else {
// 		return "pop";
// 	}
// }    
//

function fzbzCalculator(num){
	if (num % 15 == 0) {
		return num + " -> FizzBuzz";
	} else if (num % 5 == 0) {
		return num + " -> Buzz";
	} else if (num % 3 == 0) {  
		return num + " -> Fizz";
	} else {
		return num + " -> pop";
	}
}

console.log(fzbzCalculator(5));
console.log(fzbzCalculator(30));
console.log(fzbzCalculator(27));
console.log(fzbzCalculator(17));


/*
2. Make a function that gets a letter from roygbiv and it returns the corresponding color. 
(r -> Red, o -> Orange, etc.) Make sure that you sanitize your input

Test cases:
r -> Red
G -> Green
x -> No color
B -> Blue
i -> Indigo
*/

function colorPicker(letter){
	let letterCleaned = letter.toLowerCase();

	if(letterCleaned == "r"){
		return  letter +" -> Red";
	} else if (letterCleaned == "g"){
		return letter + " -> Green";
	} else if (letterCleaned == "b"){
		return letter + " -> Blue";
	} else if (letterCleaned == "i"){
		return letter + " -> Indigo";
	} else {
		return letter + " -> No color";
	}
}

console.log(colorPicker("r"));
console.log(colorPicker("G"));
console.log(colorPicker("x"));
console.log(colorPicker("B"));
console.log(colorPicker("i"));

/*
3. (Stretch goal) Make a function that determines if a year is a leap year or not.

Test case:
1900 -> Not a leap year
2000 -> Leap year
2004 -> Leap year
2021 -> Not a leap year
*/

function leapYearCalculator(year){
	if (((year % 4 == 0 && year % 100 != 0)) || year % 400 == 0) {
		return year + " -> Leap year";
	} else {
		return year +" -> Not a leap year";
	} 
}

console.log(leapYearCalculator(1900));
console.log(leapYearCalculator(2000));
console.log(leapYearCalculator(2004));
console.log(leapYearCalculator(2021));



